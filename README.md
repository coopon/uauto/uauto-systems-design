<div width="100%" align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/2322782/horn-colored.png" width="200" />
  <h2>The open-platkit for building alternative Social Transport System</h2>
</div>

[![GPLV3 license](https://img.shields.io/badge/license-GPLV3-blue.svg)](https://gitlab.com/coopon/uauto/uauto-system-design/blob/master/LICENSE) [![Coopon](https://img.shields.io/badge/developers-coopon-red.svg)](https://wiki.coopon.scitech.in)